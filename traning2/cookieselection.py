# https://www.youtube.com/watch?v=ldursSg6cH8
# It is good to smile every once in a while
from sys import stdin
lines = [x.strip() for x in stdin.readlines()]

from heapq import *

lower = []
upper = []

for line in lines:
    if line == "#":
        if len(lower) <= len(upper):
            print(heappop(upper))
        else:
            print(-heappop(lower))
    else:
        num = int(line)
        # 
        if len(lower) <= len(upper):
            if upper and upper[0] < num:
                # Swap smallest in upper with num
                tmp = num
                num = heappop(upper)
                heappush(upper, tmp)
            heappush(lower, -num)
        else:
            if lower and -lower[0] > num:
                # Swap biggest in lower with num
                tmp = num
                num = -heappop(lower)
                heappush(lower, -tmp)
            heappush(upper, num)
            
