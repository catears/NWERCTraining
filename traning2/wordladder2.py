from collections import deque
from sys import stdin
N = int(stdin.readline())
words = [stdin.readline().strip() for _ in range(N)]
start_word = words[0]
end_word = words[1]

graph = {}

for word in words:
    graph[word] = []

def replace(word, index, char):
    return word[:index] + char + word[index+1:]

def connects(a, b):
    idx = []
    for index in range(len(a)):
        if a[index] != b[index]:
            idx.append(index)
    return idx

def bridge(a, b):
    idx = connects(a, b)
    if len(idx) == 2:
        # Exactly two letters differ, either we change the first one or the second one
        a_idx, b_idx = idx
        cand_a = replace(a, a_idx, b[a_idx])
        cand_b = replace(a, b_idx, b[b_idx])
        return min(cand_a, cand_b)
    else:
        return None

for i in range(N):
    for k in range(i + 1, N):
        a, b = words[i], words[k]
        connect = connects(a, b)
        if len(connect) == 1:
            graph[a].append(b)
            graph[b].append(a)

end_distance = {end_word: 0}
queue = deque([end_word])
while queue:
    top = queue.popleft()
    dist = end_distance[top]
    for neigh in graph[top]:
        if neigh not in end_distance:
            queue.append(neigh)
            end_distance[neigh] = dist + 1

INF = 10000000000
start_distance = {start_word: 0}
queue = deque([start_word])
best = (INF, "sentinel")
if start_word in end_distance:
    # There is a road from start to end
    best = end_distance[start_word], ""

while queue:
    top = queue.popleft()
    dist = start_distance[top]
    for key in end_distance:
        b = bridge(top, key)
        if b:
            added = end_distance[key]
            best = min(best, (dist + added + 2, b))
    for neigh in graph[top]:
        if neigh not in start_distance:
            queue.append(neigh)
            start_distance[neigh] = dist + 1


if best[1] == "":
    print(0)
    print(best[0])
elif best[0] == INF:
    print(0)
    print(-1)
else:
    print(best[1])
    print(best[0])
