#include <bits/stdc++.h>

using namespace std;

int memo[10005][105];
int bills[10005][105];
int B[10005];
int INF = 100000000;

int sentinel = INF + 1;

int DP(int p, int idx, int Len) {
    if (p <= 0) {
        return p;
    } else if (idx >= Len) {
        return INF;
    } else {
        if (memo[p][idx] != sentinel) {
            return memo[p][idx];
        }

        int a = DP(p - B[idx], idx + 1, Len);
        int b = DP(p, idx + 1, Len);
        int value, num;
        if (a == INF and b == INF) {
            value = INF;
            num = 0;
        } else if (a == INF) {
            value = b;
            num = bills[p][idx + 1];
        } else if (b == INF) {
            value = a;
            if (p - B[idx] <= 0) {
                num = 1;
            } else {
                num = bills[p - B[idx]][idx + 1] + 1;
            }
        } else if (abs(a) < abs(b)) {
            value = a;
            if (p - B[idx] <= 0) {
                num = 1;
            } else {
                num = bills[p - B[idx]][idx + 1] + 1;
            }
        } else if (abs(a) == abs(b)) {
            int numa, numb;
            if (p - B[idx] <= 0) {
                numa = 1;
            } else {
                numa = bills[p - B[idx]][idx + 1] + 1;
            }
            numb = bills[p][idx + 1];
            if (numa < numb) {
                value = a;
                num = numa;
            } else {
                value = b;
                num = numb;
            }
        } else {
            value = b;
            num = bills[p][idx + 1];
        }
        memo[p][idx] = value;
        bills[p][idx] = num;
        return value;
    }
}

void solve() {
    int P, N; cin >> P >> N;
    for (int i = 0; i < N; ++i) cin >> B[i];
    for (int i = 0; i < 10005; ++i) {
        for (int j = 0; j < 105; ++j) {
            memo[i][j] = sentinel;
            bills[i][j] = 0;
        }
    }
    DP(P, 0, N);

    int overpaid = memo[P][0];
    int num_bills = bills[P][0];
    cout << P - overpaid << " " << num_bills << endl;
}

int main() {
    int TC; cin >> TC;
    while (TC--) {
        solve();
    }
}
